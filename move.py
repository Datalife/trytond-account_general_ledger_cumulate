# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.transaction import Transaction
from sql import Literal


class Line(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    @classmethod
    def query_get(cls, table):
        line_query, fiscalyear_ids = super().query_get(table)

        context = Transaction().context
        date = context.get('date')
        from_date, to_date = context.get('from_date'), context.get('to_date')
        fiscalyear_id = context.get('fiscalyear')
        period_ids = context.get('periods')
        if (context.get('closed_fiscalyears', False)
                and not date
                and not fiscalyear_id
                and period_ids is None
                and not from_date
                and not to_date):
            # remove open fiscal year condition
            new_where = Literal(True) & list(line_query.right.where)[1]
            line_query.right.where = new_where
        return line_query, fiscalyear_ids
