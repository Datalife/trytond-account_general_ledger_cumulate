# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import datetime
from decimal import Decimal
from trytond.pool import Pool
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.transaction import Transaction

from trytond.modules.company.tests import create_company, set_company
from trytond.modules.currency.tests import create_currency
from trytond.modules.account.tests import get_fiscalyear, create_chart
from trytond.modules.account.tests.test_module import close_fiscalyear


class AccountGeneralLedgerCumulateTestCase(ModuleTestCase):
    """Test Account General Ledger Cumulate module"""
    module = 'account_general_ledger_cumulate'

    @with_transaction()
    def test_account_debit_credit(self):
        'Test account debit/credit'
        pool = Pool()
        Party = pool.get('party.party')
        FiscalYear = pool.get('account.fiscalyear')
        Journal = pool.get('account.journal')
        Account = pool.get('account.account')
        Move = pool.get('account.move')
        GeneralLedger = pool.get('account.general_ledger.account')
        GeneralLedgerLine = pool.get('account.general_ledger.line')

        database = Transaction().database

        party = Party(name='Party')
        party.save()

        company = create_company()
        with set_company(company):
            fiscalyear = get_fiscalyear(company)
            fiscalyear.save()
            FiscalYear.create_period([fiscalyear])
            period = fiscalyear.periods[0]
            create_chart(company)

            sec_cur = create_currency('sec')

            journal_revenue, = Journal.search([
                    ('code', '=', 'REV'),
                    ])
            journal_expense, = Journal.search([
                    ('code', '=', 'EXP'),
                    ])
            revenue, = Account.search([
                    ('type.revenue', '=', True),
                    ])
            receivable, = Account.search([
                    ('type.receivable', '=', True),
                    ])
            expense, = Account.search([
                    ('type.expense', '=', True),
                    ])
            payable, = Account.search([
                    ('type.payable', '=', True),
                    ])
            cash, = Account.search([
                    ('name', '=', 'Main Cash'),
                    ])
            cash_cur, = Account.copy([cash], default={
                    'second_currency': sec_cur.id,
                    })
            # Create some moves
            vlist = [
                {
                    'period': period.id,
                    'journal': journal_revenue.id,
                    'date': period.start_date,
                    'lines': [
                        ('create', [{
                                    'account': revenue.id,
                                    'credit': Decimal(100),
                                    }, {
                                    'account': receivable.id,
                                    'debit': Decimal(100),
                                    'party': party.id,
                                    }]),
                        ],
                    },
                {
                    'period': period.id,
                    'journal': journal_revenue.id,
                    'date': period.start_date,
                    'lines': [
                        ('create', [{
                                    'account': receivable.id,
                                    'credit': Decimal(80),
                                    'second_currency': sec_cur.id,
                                    'amount_second_currency': -Decimal(50),
                                    'party': party.id,
                                    }, {
                                    'account': cash_cur.id,
                                    'debit': Decimal(80),
                                    'second_currency': sec_cur.id,
                                    'amount_second_currency': Decimal(50),
                                    }]),
                        ],
                    },
                {
                    'period': period.id,
                    'journal': journal_expense.id,
                    'date': period.start_date,
                    'lines': [
                        ('create', [{
                                    'account': expense.id,
                                    'debit': Decimal(30),
                                    }, {
                                    'account': payable.id,
                                    'credit': Decimal(30),
                                    'party': party.id,
                                    }]),
                        ],
                    },
                ]
            Move.create(vlist)

            # Test start balance / end balance
            for fyear in (fiscalyear.id, None):
                with Transaction().set_context(fiscalyear=fyear):
                    revenue = GeneralLedger(revenue.id)
                    self.assertEqual((revenue.debit, revenue.credit),
                        (Decimal(0), Decimal(100)))
                    self.assertEqual(revenue.start_balance, Decimal(0))
                    self.assertEqual(revenue.end_balance, Decimal(-100))

            # Use next fiscalyear
            today = datetime.date.today()
            next_fiscalyear = get_fiscalyear(company,
                today=today.replace(year=today.year + 1))
            next_fiscalyear.save()
            FiscalYear.create_period([next_fiscalyear])

            # Test start balance / end balance for next year
            # standard query
            with Transaction().set_context(fiscalyear=next_fiscalyear.id):
                revenue = GeneralLedger(revenue.id)
                self.assertEqual((revenue.debit, revenue.credit),
                    (Decimal(0), Decimal(0)))
                self.assertEqual(revenue.start_balance, Decimal(-100))
                self.assertEqual(revenue.end_balance, Decimal(-100))

            # Test start balance / end balance for next year
            # query with no filters: neither fiscalyear nor dates
            with Transaction().set_context(fiscalyear=None):
                revenue = GeneralLedger(revenue.id)
                self.assertEqual((revenue.debit, revenue.credit),
                    (Decimal(0), Decimal(100)))
                self.assertEqual(revenue.start_balance, Decimal(0))
                self.assertEqual(revenue.end_balance, Decimal(-100))

            # Test start balance / end balance for next year
            # query without fiscalyear and with from_date
            with Transaction().set_context(fiscalyear=None,
                    from_date=next_fiscalyear.periods[0].start_date):
                revenue = GeneralLedger(revenue.id)
                self.assertEqual((revenue.debit, revenue.credit),
                    (Decimal(0), Decimal(0)))
                self.assertEqual(revenue.start_balance, Decimal(-100))
                self.assertEqual(revenue.end_balance, Decimal(-100))

            # Create some other moves
            vlist = [
                {
                    'period': fiscalyear.periods[2].id,
                    'journal': journal_revenue.id,
                    'date': fiscalyear.periods[2].start_date,
                    'lines': [
                        ('create', [{
                                    'account': revenue.id,
                                    'credit': Decimal(60),
                                    }, {
                                    'account': receivable.id,
                                    'debit': Decimal(60),
                                    'party': party.id,
                                    }]),
                        ],
                    },
                {
                    'period': next_fiscalyear.periods[1].id,
                    'journal': journal_revenue.id,
                    'date': next_fiscalyear.periods[1].start_date,
                    'lines': [
                        ('create', [{
                                    'account': revenue.id,
                                    'credit': Decimal(70),
                                    }, {
                                    'account': receivable.id,
                                    'debit': Decimal(70),
                                    'party': party.id,
                                    }]),
                        ],
                    },
                ]
            Move.create(vlist)

            # check general ledger lines with fiscalyear
            with Transaction().set_context(fiscalyear=next_fiscalyear.id):
                lines = GeneralLedgerLine.search([
                    ('account', '=', revenue.id)])
                self.assertEqual(len(lines), 1)
                line = lines[0]
                if database.has_window_functions():
                    self.assertEqual(line.balance, Decimal(-230))
                else:
                    self.assertEqual(line.balance, Decimal(-70))

            # check general ledger lines without fiscal year
            with Transaction().set_context(fiscalyear=None):
                lines = GeneralLedgerLine.search([
                    ('account', '=', revenue.id)])
                self.assertEqual(len(lines), 3)
                for line in lines:
                    if line.move.period.fiscalyear == next_fiscalyear:
                        break
                if database.has_window_functions():
                    self.assertEqual(line.balance, Decimal(-230))
                else:
                    self.assertEqual(line.balance, Decimal(-70))

            close_fiscalyear(fiscalyear)

            # Test debit/credit
            with Transaction().set_context(fiscalyear=fiscalyear.id):
                revenue = Account(revenue.id)
                self.assertEqual((revenue.debit, revenue.credit),
                    (Decimal(160), Decimal(160)))
                self.assertEqual(revenue.balance, Decimal(0))
                revenue = GeneralLedger(revenue.id)
                self.assertEqual((revenue.debit, revenue.credit),
                    (Decimal(160), Decimal(160)))
                self.assertEqual(revenue.start_balance, Decimal(0))
            with Transaction().set_context(fiscalyear=fiscalyear.id,
                    end_period=fiscalyear.periods[2]):
                # this does not work without end period
                revenue = GeneralLedger(revenue.id)
                self.assertEqual(revenue.end_balance, Decimal(-160))

            # Test debit/credit for next year
            with Transaction().set_context(fiscalyear=next_fiscalyear.id):
                revenue = Account(revenue.id)
                self.assertEqual((revenue.debit, revenue.credit),
                    (Decimal(0), Decimal(70)))
                self.assertEqual(revenue.balance, Decimal(-70))
                revenue = GeneralLedger(revenue.id)
                self.assertEqual((revenue.debit, revenue.credit),
                    (Decimal(0), Decimal(70)))
                self.assertEqual(revenue.start_balance, Decimal(0))
                self.assertEqual(revenue.end_balance, Decimal(-70))

            # Test debit/credit for non year
            with Transaction().set_context(fiscalyear=None,
                    from_date=next_fiscalyear.periods[1].start_date):
                revenue = GeneralLedger(revenue.id)
                self.assertEqual((revenue.debit, revenue.credit),
                    (Decimal(0), Decimal(70)))
                self.assertEqual(revenue.start_balance, Decimal(0))
                self.assertEqual(revenue.end_balance, Decimal(-70))

            # Test debit/credit by date overlapping fiscalyears
            with Transaction().set_context(fiscalyear=None,
                    from_date=fiscalyear.periods[1].end_date):
                revenue = GeneralLedger(revenue.id)
                self.assertEqual((revenue.debit, revenue.credit),
                    (Decimal(160), Decimal(130)))
                self.assertEqual(revenue.start_balance, Decimal(-100))
                self.assertEqual(revenue.end_balance, Decimal(-70))
            with Transaction().set_context(fiscalyear=None,
                    from_date=fiscalyear.periods[1].end_date,
                    to_date=next_fiscalyear.periods[0].end_date):
                revenue = GeneralLedger(revenue.id)
                self.assertEqual((revenue.debit, revenue.credit),
                    (Decimal(160), Decimal(60)))
                self.assertEqual(revenue.start_balance, Decimal(-100))
                self.assertEqual(revenue.end_balance, Decimal(0))

            # check general ledger lines with fiscalyear
            with Transaction().set_context(fiscalyear=next_fiscalyear.id):
                lines = GeneralLedgerLine.search([
                    ('account', '=', revenue.id)])
                self.assertEqual(len(lines), 1)
                line = lines[0]
                self.assertEqual(line.balance, Decimal(-70))

            # check general ledger lines without fiscal year
            with Transaction().set_context(fiscalyear=None):
                lines = GeneralLedgerLine.search([
                    ('account', '=', revenue.id)])
                self.assertEqual(len(lines), 4)
                for line in lines:
                    if line.move.period.fiscalyear == next_fiscalyear:
                        break
                self.assertEqual(line.balance, Decimal(-70))


del ModuleTestCase
